package com.openbravo.sharaf.retail.creditnotes.event;

import java.util.Calendar;

import javax.enterprise.event.Observes;

import org.openbravo.base.model.Entity;
import org.openbravo.base.model.ModelProvider;
import org.openbravo.base.model.Property;
import org.openbravo.client.kernel.event.EntityNewEvent;
import org.openbravo.client.kernel.event.EntityPersistenceEventObserver;
import org.openbravo.retail.giftcards.org.openbravo.retail.giftcards.GiftCardInst;

public class GiftCardInstEventHandler extends EntityPersistenceEventObserver {
  private static Entity[] entities = { ModelProvider.getInstance().getEntity(
      GiftCardInst.ENTITY_NAME) };

  @Override
  protected Entity[] getObservedEntities() {
    return entities;
  }

  public void onSave(@Observes EntityNewEvent event) {
    if (!isValidEvent(event)) {
      return;
    }
    GiftCardInst giftCardInst = (GiftCardInst) event.getTargetInstance();
    if ("BasedOnCreditNote".equals(giftCardInst.getType())) {
      Calendar cal = Calendar.getInstance();
      Long days = giftCardInst.getSalesOrder().getOrganization().getCustcnExpireDays();
      if (days == null) {
        days = giftCardInst.getOrganization().getCustcnExpireDays();
        if (days == null) {
          days = 180L;
        }
      }
      cal.add(Calendar.DAY_OF_YEAR, days.intValue());
      final Entity giftCardInstEntity = ModelProvider.getInstance().getEntity(
          GiftCardInst.ENTITY_NAME);
      final Property giftCardInstProperty = giftCardInstEntity.getProperty("obgcneExpirationdate");
      event.setCurrentState(giftCardInstProperty, cal.getTime());
    }
  }
}
