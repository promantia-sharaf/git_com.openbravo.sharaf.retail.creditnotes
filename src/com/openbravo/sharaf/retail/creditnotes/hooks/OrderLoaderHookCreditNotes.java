/*
 ************************************************************************************
 * Copyright (C) 2017-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.creditnotes.hooks;

import java.math.BigDecimal;

import javax.enterprise.context.ApplicationScoped;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.retail.giftcards.GiftCardModel;
import org.openbravo.retail.giftcards.org.openbravo.retail.giftcards.GiftCardInst;
import org.openbravo.retail.giftcards.process.GiftCardGLItemUtils;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.OrderLoaderPaymentHook;

@ApplicationScoped
public class OrderLoaderHookCreditNotes extends OrderLoaderPaymentHook {

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {
    // Process Credit Note Payments
    BusinessPartner bp = order.getBusinessPartner();
    Organization org = OBDal.getInstance().get(Organization.class,
        jsonorder.getString("organization"));

    JSONArray payments = jsonorder.getJSONArray("payments");
    for (int i = 0; i < payments.length(); i++) {
      final JSONObject payment = payments.getJSONObject(i);
      final BigDecimal origAmount = new BigDecimal(payment.getString("origAmount"));
      if ("GCNV_payment.creditnote".equals(payment.getString("kind")) && payment.has("paymentData")
          && origAmount.compareTo(BigDecimal.ZERO) > 0) {
        String cardNumber = payment.getJSONObject("paymentData").getJSONObject("creditNote")
            .getString("searchKey");
        GiftCardModel model = new GiftCardModel();
        GiftCardInst cardInst = model.findGiftCard(cardNumber, "G");
        if (cardInst != null && !"C".equals(cardInst.getAlertStatus())) {
          // Close Credit Note
          BigDecimal amount = cardInst.getAmount();
          OBPOSApplications terminal = OBDal.getInstance().get(OBPOSApplications.class,
              jsonorder.getString("posTerminal"));

          GiftCardGLItemUtils.close(terminal, cardInst.getId(), "E");

          // Adjust searckKey
          OBCriteria<GiftCardInst> obc = OBDal.getInstance().createCriteria(GiftCardInst.class);
          obc.add(Restrictions.ilike(GiftCardInst.PROPERTY_SEARCHKEY, cardInst.getSearchKey(),
              MatchMode.START));
          obc.addOrderBy(GiftCardInst.PROPERTY_SEARCHKEY, false);
          obc.setMaxResults(1);
          GiftCardInst lastCreditNote = (GiftCardInst) obc.uniqueResult();
          String searchKey = lastCreditNote.getSearchKey();
          int indx = searchKey.lastIndexOf("*C");
          if (indx > 0) {
            int last = Integer.parseInt(lastCreditNote.getSearchKey().substring(indx + 2));
            cardInst.setSearchKey(searchKey.substring(0, indx) + "*C" + (last + 1));
          } else {
            cardInst.setSearchKey(searchKey + "*C1");
          }
          OBDal.getInstance().save(lastCreditNote);

          // Create new Credit Note
          BigDecimal paymentAmount = new BigDecimal(payment.getString("amount"));
          amount = amount.subtract(paymentAmount);
          final FIN_Payment origPayment = OBDal.getInstance().get(FIN_Payment.class,
              payment.getString("id"));
          GiftCardInst creditNote = model.createCreditNote(org, cardInst.getId(), bp,
              order.getOrderDate(), order, null, amount, origPayment);
          creditNote.setSearchKey(cardInst.getReturnCOrder().getDocumentNo());
          creditNote.setReturnCOrder(cardInst.getReturnCOrder());
          OBDal.getInstance().save(creditNote);
        }
      }
    }
  }

}
