package com.openbravo.sharaf.retail.creditnotes;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.retail.posterminal.OrderLoaderPreProcessPaymentHook;
import org.apache.log4j.Logger;

public class CreditNoteOrderLoaderPreProcessPaymentHook extends OrderLoaderPreProcessPaymentHook {

  @Override
  public void exec(JSONObject jsonorder, Order order, JSONObject jsonpayment, FIN_Payment payment)
      throws Exception {
    Logger log = Logger.getLogger(CreditNoteOrderLoaderPreProcessPaymentHook.class);
  try {
    if (jsonpayment.has("creditnoteId")) {
      String creditnotenumber = jsonpayment.getString("creditnoteId");
      payment.setCustcnCreditnoteNumber(creditnotenumber);
    }
    String searchkey = null;
    if(jsonpayment.has("paymentData") && jsonpayment.getJSONObject("paymentData").has("creditNote")) {
     searchkey = jsonpayment.getJSONObject("paymentData").getJSONObject("creditNote").getString("searchKey");
    }
    if (searchkey != "") {
      payment.setCustcnCreditnoteNumber(searchkey);
    }
 } catch(Exception e) {
   log.error("Exception while adding creditnotenumber in CreditNoteOrderLoaderPreProcessPaymentHook.java ", e);
  }
 }
}  