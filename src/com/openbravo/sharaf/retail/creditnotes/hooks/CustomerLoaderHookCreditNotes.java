/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.creditnotes.hooks;

import org.codehaus.jettison.json.JSONObject;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.retail.posterminal.CustomerLoaderHook;

public class CustomerLoaderHookCreditNotes implements CustomerLoaderHook {

  @Override
  public void exec(JSONObject jsonCustomer, BusinessPartner customer) throws Exception {
    customer.setGcnvUniquecreditnote(false);
  }

}
