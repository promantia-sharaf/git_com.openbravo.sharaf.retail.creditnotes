/*
 ************************************************************************************
 * Copyright (C) 2018-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.creditnotes.process;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.retail.posterminal.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;

public class GetPaymentsForReceiptLines extends JSONProcessSimple {

  @Override
  public JSONObject exec(JSONObject request) {
    JSONObject json = request.optJSONObject("parameters");
    JSONObject response = new JSONObject();
    JSONObject data = new JSONObject();
    OBContext.setAdminMode(true);
    try {
      response.put("data", data);
      response.put("status", JsonConstants.RPCREQUEST_STATUS_SUCCESS);
      JSONArray payments = new JSONArray();
      data.put("payments", payments);

      JSONArray orderLines = json.getJSONArray("orderLines");
      for (int i = 0; i < orderLines.length(); i++) {
        OrderLine line = OBDal.getInstance().get(OrderLine.class, orderLines.get(i).toString());
        if (line != null) {
          addPayments(line.getSalesOrder(), payments);
        }
      }
    } catch (Exception e) {
      // Ignore
    } finally {
      OBContext.restorePreviousMode();
    }
    JSONObject result = new JSONObject();
    try {
      result.put(JsonConstants.RESPONSE_DATA, response);
      result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (JSONException ignoree) {
    }
    return result;
  }

  private void addPayments(Order order, JSONArray payments) throws JSONException {
    // Ignore orders if is already in payments
    for (int i = 0; i < payments.length(); i++) {
      JSONObject item = payments.getJSONObject(i);
      if (item.getString("ticket").equals(order.getDocumentNo())) {
        return;
      }
    }

    JSONArray orderPayments = new JSONArray();
    JSONObject jsonOrder = new JSONObject();
    jsonOrder.put("ticket", order.getDocumentNo());
    jsonOrder.put("payments", orderPayments);

    CreditNoteUtils.addPayments(order, orderPayments);

    payments.put(jsonOrder);
  }
}
