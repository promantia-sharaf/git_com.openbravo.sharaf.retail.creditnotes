/*
 ************************************************************************************
 * Copyright (C) 2017 -2018Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.creditnotes.process;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOutLine;
import org.openbravo.retail.giftcards.FindGiftCard;
import org.openbravo.retail.giftcards.GiftCardModel;
import org.openbravo.retail.giftcards.org.openbravo.retail.giftcards.GiftCardInst;
import org.openbravo.service.json.JsonConstants;

public class FindGiftCreditNote extends FindGiftCard {

  @Override
  protected JSONObject execute(JSONObject json) {
    JSONObject result = super.execute(json);
    try {
      if (JsonConstants.RPCREQUEST_STATUS_SUCCESS == (Integer) result
          .get(JsonConstants.RESPONSE_STATUS)) {
        GiftCardModel model = new GiftCardModel();
        JSONArray payments = new JSONArray();
        ((JSONObject) result.get("data")).put("payments", payments);
        final GiftCardInst giftcard = model.findGiftCard(json.getString("giftcard"),
            json.optString("giftcardtype", null));
        if (giftcard != null && "BasedOnCreditNote".equals(giftcard.getType())) {
          Map<String, String> processedOrder = new HashMap<String, String>();
          Order returnOrder = giftcard.getReturnCOrder();
          for (OrderLine line : returnOrder.getOrderLineList()) {
            ShipmentInOutLine inOutLine = line.getGoodsShipmentLine();
            if (inOutLine != null) {
              Order originalOrder = inOutLine.getSalesOrderLine().getSalesOrder();
              if (!processedOrder.containsKey(originalOrder.getId())) {
                processedOrder.put(originalOrder.getId(), originalOrder.getDocumentNo());
                JSONArray orderPayments = new JSONArray();
                JSONObject order = new JSONObject();
                order.put("ticket", originalOrder.getDocumentNo());
                order.put("payments", orderPayments);
                payments.put(order);

                CreditNoteUtils.addPayments(originalOrder, orderPayments);
              }
            }
          }
        }
      }
    } catch (Exception e) {
      // Ignore
    }
    return result;
  }

}
