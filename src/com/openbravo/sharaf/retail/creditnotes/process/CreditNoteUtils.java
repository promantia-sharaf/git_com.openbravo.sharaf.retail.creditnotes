/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.creditnotes.process;

import java.math.RoundingMode;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentSchedule;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.retail.posterminal.TerminalType;
import org.openbravo.retail.posterminal.TerminalTypePaymentMethod;

public class CreditNoteUtils {

  public static void addPayments(Order order, JSONArray orderPayments) throws JSONException {
    TerminalType terminalType = order.getObposApplications().getObposTerminaltype();
    List<TerminalTypePaymentMethod> paymentMethodList = terminalType.getOBPOSAppPaymentTypeList();

    for (FIN_PaymentSchedule paymentSchedule : order.getFINPaymentScheduleList()) {
      for (FIN_PaymentScheduleDetail paymentScheduleDetail : paymentSchedule
          .getFINPaymentScheduleDetailOrderPaymentScheduleList()) {
        JSONObject payment = new JSONObject();
        payment.put("name", getPaymentMethodName(paymentMethodList, paymentScheduleDetail));
        payment.put("amount", paymentScheduleDetail.getAmount());
        payment.put(
            "origAmount",
            paymentScheduleDetail
                .getAmount()
                .multiply(
                    paymentScheduleDetail.getPaymentDetails().getFinPayment()
                        .getFinancialTransactionConvertRate())
                .setScale(
                    paymentScheduleDetail.getPaymentDetails().getFinPayment().getAccount()
                        .getCurrency().getStandardPrecision().intValue(), RoundingMode.HALF_UP));

        orderPayments.put(payment);
      }
    }
  }

  private static String getPaymentMethodName(List<TerminalTypePaymentMethod> paymentMethodList,
      FIN_PaymentScheduleDetail scheduleDetail) {
    FIN_Payment finPayment = scheduleDetail.getPaymentDetails().getFinPayment();
    FIN_PaymentMethod finPaymentMethod = scheduleDetail.getAPRMPaymentMethod();
    for (TerminalTypePaymentMethod paymentMethod : paymentMethodList) {
      if (paymentMethod.getPaymentMethod().getId().equals(finPaymentMethod.getId())
          && paymentMethod.getCurrency().getId()
              .equals(finPayment.getAccount().getCurrency().getId())) {
        return paymentMethod.getName();
      }
    }
    return finPaymentMethod.getName();
  }

}
