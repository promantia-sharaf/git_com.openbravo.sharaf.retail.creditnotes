/*
 ************************************************************************************
 * Copyright (C) 2018-2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('GCNV_PrePrintGiftCard', function (args, callbacks) {
    if (args.giftCard.type === 'BasedOnCreditNote') {
      args.giftCardData.set('orderDate', args.giftCard.orderDate);
      args.giftCardData.set('created', args.giftCard.creationDate);
      if (args.originator && args.originator.kind === 'GCNV.UI.PrintButton') {
        args.giftCardData.set('custncReprintDuplicate', true);
      }
      OB.UI.GiftCardUtils.service('com.openbravo.sharaf.retail.creditnotes.process.FindGiftCreditNote', {
        giftcard: args.giftCard.searchKey
      }, function (result) {
        args.giftCardData.set('payments', result.data.payments);
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }, function (error) {
        if (error && error.exception && error.exception.message.indexOf('GCNV_ErrorGiftCardNotExists') === 0 && args.order) {
          var originalOrderLines = [];
          _.each(args.order.get('lines').models, function (line) {
            if (line.get('originalDocumentNo') && line.get('originalOrderLineId')) {
              originalOrderLines.push(line.get('originalOrderLineId'));
            }
          });
          var process = new OB.DS.Request('com.openbravo.sharaf.retail.creditnotes.process.GetPaymentsForReceiptLines');
          process.exec({
            orderLines: originalOrderLines
          }, function (result) {
            if (result && !result.exception) {
              args.giftCardData.set('payments', result.data.payments);
            }
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          }, function () {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          });
        } else {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
      });
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

}());