GCNV.UI.Details.extend({
  acceptButton: function (inSender, inEvent) {
    var giftcard = this.args.giftcard;
    var me = this;
    var negativeLines = _.filter(OB.MobileApp.model.receipt.get('lines').models, function (line) {
        if (OB.MobileApp.model.get('permissions').CUSTCN_GiftcardSearchKey && 
          OB.MobileApp.model.get('permissions').CUSTCN_GiftcardSearchKey.includes(line.get('product').attributes.searchkey )){
          return line;
        }  
      });
    if (negativeLines.length >0 && giftcard.searchKey.slice(0,2) === 'MR') {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTCN_GiftcardCreditNoteMsg'));
    } else {
      var showMessagePopup = function (message, action) {
          var keyboard = message.keyboard,
              args = {};
          args.message = message.params ? OB.I18N.getLabel(message.message, message.params) : OB.I18N.getLabel(message.message);
          if (message.header) {
            args.header = message.header;
          }
          args.callback = action;
          keyboard.doShowPopup({
            popup: 'GCNV_UI_Message',
            args: args
          });
          };

      var consumeOK = function (callback, successMessage) {
          if (me.args.okcallback) {
            me.args.okcallback();
          }
          me.statusReady();
          me.propertiesCancelled = false;
          me.doHideThisPopup();

          if (successMessage) {
            showMessagePopup(successMessage);
          }

          if (callback) {
            callback();
          }
          };

      var consumeFail = function (callback, errorMessage) {
          me.statusReady();
          me.propertiesCancelled = false;

          if (errorMessage) {
            me.doHideThisPopup();
            showMessagePopup(errorMessage, function () {
              errorMessage.keyboard.doShowPopup({
                popup: 'GCNV_UI_Details',
                args: me.args
              });
            });
          }

          me.propertiesCancelled = true;
          if (callback) {
            callback();
          }
          };

      this.statusLoading();
      this.propertiesCancelled = false;
      if (this.args.action) {
        this.args.action(this, consumeOK, consumeFail);
      } else {
        if (giftcard.amount) {
          // Is a Product Gift Card
          OB.UI.GiftCardUtils.checkIfExpiredGiftCardAndConsume(this.args.view, this.args.receipt, giftcard, this.args.amount || this.args.receipt.getPending(), this.args.receipt.get('priceIncludesTax'), consumeOK, consumeFail);
        } else {
          // Is a Gift Voucher
          OB.UI.GiftCardUtils.checkIfExpiredVoucherAndConsume(this.args.view, this.args.receipt, giftcard.searchKey, consumeOK, consumeFail, {
            cardType: 'V'
          });
        }
      }
    }
}

});