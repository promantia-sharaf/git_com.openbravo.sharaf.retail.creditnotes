/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, Backbone, _ */
(function () {

  OB.UTIL.HookManager.registerHook('OBPRINT_PrePrint', function (args, callbacks) {

    var creditNodePayments = [],
        payments = args.order.get('payments').models;

    _.each(payments, function (payment) {
      if (payment.get('kind') === 'GCNV_payment.creditnote') {
        creditNodePayments.push(payment);
      }
    });

    var finishPrint = function () {
        var ok = true;
        _.each(creditNodePayments, function (payment) {
          if (payment.get('pending')) {
            ok = false;
          }
        });
        if (ok) {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
        };

    if (creditNodePayments.length > 0 && !OB.MobileApp.model.get('terminal').shfptDisbalePrint) {
      var gctemplateresource = new OB.DS.HWResource(OB.MobileApp.model.get('terminal').printCreditNoteTemplate || "../org.openbravo.retail.giftcards/res/creditnote.xml");
      _.each(creditNodePayments, function (payment) {
        var hasPaymentData = !OB.UTIL.isNullOrUndefined(payment.get('paymentData'));
        var printTwice = payment.get('printtwice');
        payment.set('pending', hasPaymentData);
        if (hasPaymentData) {
          OB.UI.GiftCardUtils.service('com.openbravo.sharaf.retail.creditnotes.process.FindGiftCreditNote', {
            giftcard: payment.get('paymentData').card
          }, function (result) {
            var currentamount = result.data.currentamount;
            if (!OB.UTIL.isNullOrUndefined(printTwice) && printTwice) {
              var giftCard = new Backbone.Model({
                giftCardId: payment.get('paymentData').card,
                businessPartnerName: args.order.get('bp').get('_identifier'),
                currentamount: currentamount,
                payments: result.data.payments
              });
              OB.POS.hwserver.print(gctemplateresource, {
                giftCardData: giftCard
              }, function () {
                payment.set('pending', false);
                finishPrint();
              });
            } else {
              payment.set('pending', false);
              finishPrint();
            }
          }, function () {
            payment.set('pending', false);
            finishPrint();
          });
        } else {
          finishPrint();
        }
      });
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

}());
